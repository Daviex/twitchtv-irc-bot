﻿namespace TwitchTV_Bot
{
    partial class ViewerList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_viewers = new System.Windows.Forms.ListBox();
            this.lbl_text = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lst_viewers
            // 
            this.lst_viewers.BackColor = System.Drawing.Color.Lime;
            this.lst_viewers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lst_viewers.FormattingEnabled = true;
            this.lst_viewers.ItemHeight = 20;
            this.lst_viewers.Location = new System.Drawing.Point(-2, 38);
            this.lst_viewers.Name = "lst_viewers";
            this.lst_viewers.Size = new System.Drawing.Size(294, 664);
            this.lst_viewers.TabIndex = 0;
            // 
            // lbl_text
            // 
            this.lbl_text.AutoSize = true;
            this.lbl_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_text.Location = new System.Drawing.Point(61, -2);
            this.lbl_text.Name = "lbl_text";
            this.lbl_text.Size = new System.Drawing.Size(151, 31);
            this.lbl_text.TabIndex = 1;
            this.lbl_text.Text = "Viewers: 0";
            // 
            // ViewerList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 709);
            this.Controls.Add(this.lbl_text);
            this.Controls.Add(this.lst_viewers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ViewerList";
            this.Text = "Viewer List";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lst_viewers;
        private System.Windows.Forms.Label lbl_text;
    }
}