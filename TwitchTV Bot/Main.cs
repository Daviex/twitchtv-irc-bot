﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TwitchTV_Bot
{
    public class IrcBot
    {
        public const string Server = "irc.twitch.tv";   //Irc server to connect 
        private const int Port = 6667;  //Irc server's port (6667 is default port)

        private struct StartInfo
        {
            public string Nick;   // Nick information defined in RFC 2812 (Internet Relay Chat: Client Protocol) is sent to irc server 
            public string Password;    // User password OAUTH for Twitch
            public string Channel;   // Channel to join
            public bool WantTimedMessage;   //We want it or no?
            public string TimedMessage;   //Timing Message
            public int TimerForMessage; //How soon do you want it to appear?
            public bool SpamFilterStatus;  //Spam Filter Status
            public bool JoinNotice; //Notice in chat on Join
            public bool PartNotice; //Notice in chat on Part
        };

        private static StartInfo _StartInfo;

        public static IrcWriter Irc;    // StreamWriter is declared here so that PingSender can access it

        public static List<string> GiveawayUsers;   //Declaration of a list of string that will contains users that will join the giveaway

        private struct CommandInfo  //Struct for custom commands
        {
            public string commandName;  //Name of the command
            public string text;         //Text that will be printed out
        };
        private static List<CommandInfo> ComInfo;   //Declaration of a list of CommandInfo struct that will contains all the custom commands

        private struct Warning
        {
            public string name;
            public int timeoutCount;
        };
        private static List<Warning> WarningList;  

        public static ViewerList viewers;
        
        private const string _StartInfoFile = "Content/startInfo.json"; //File with info to start up the bot
        private const string _StaffListFile = "Content/staffList.json"; //File of the staff list
        private const string _CustomCommandsFile = "Content/customCommands.json";   //File with custom commands
        
        public static void Main()
        {
            Console.Title = "QAIR Soft - TwitchTV Bot";

            ConsoleIrc.WriteHeader(@"         ____            _____ _____        _____  ____  ______ _______ ");
            ConsoleIrc.WriteHeader(@"        / __ \     /\   |_   _|  __ \      / ____|/ __ \|  ____|__   __|");
            ConsoleIrc.WriteHeader(@"       | |  | |   /  \    | | | |__) |    | (___ | |  | | |__     | |   ");
            ConsoleIrc.WriteHeader(@"       | |  | |  / /\ \   | | |  _  /      \___ \| |  | |  __|    | |   ");
            ConsoleIrc.WriteHeader(@"       | |__| | / ____ \ _| |_| | \ \      ____) | |__| | |       | |   ");
            ConsoleIrc.WriteHeader(@"        \___\_\/_/    \_\_____|_|  \_\    |_____/ \____/|_|       |_|   ");
            ConsoleIrc.WriteHeader(@"                                                                        ");
            ConsoleIrc.WriteHeader(@"                                                                        ");

            /*
             * I'm going to check if the file account exist, if it exist,
             * I read all the info about username and password and initialize variables to
             * that values. If it doesn't exist, the program close.
             */
            if (File.Exists(_StartInfoFile) && new FileInfo(_StartInfoFile).Length != 0)
            {
                _StartInfo = JsonConvert.DeserializeObject<StartInfo>(File.ReadAllText(_StartInfoFile));
            }
            else
            {
                Console.WriteLine("Error: there's no file with account info for the bot!");
                Console.ReadLine();
                Environment.Exit(0);
            }

            try
            {
                #region Client Start Region

                //Declaration of TcpClient to communicate with IrcServer, I connect to IRC with Server and Port
                TcpClient client = new TcpClient(Server, Port);
                //Client will send packets immediately when receive a network write!
                client.NoDelay = true;
                //NetworkStream is to allocate a space to write things to send to IRC, I took Stream was assigned to me
                NetworkStream stream = client.GetStream();
                //I will receive there messages from IRC, initialize the Reader
                StreamReader reader = new StreamReader(stream);
                Irc = new IrcWriter(stream, _StartInfo.Channel); //Initialize the Writer

                Irc.SendPassword(_StartInfo.Password); //Send message to tell server the password
                Irc.SendNick(_StartInfo.Nick); //Send message to tell server the nickname
                Irc.JoinChannel(); //Send message to tell server what channel to join

                #endregion

                #region Ping Region

                PingSender ping = new PingSender();
                ping.Start(); // Start PingSender thread

                #endregion

                #region References Region

                string inputLine; //Will be used in the while to keep track of lines sended by the server
                string keyword = null; //Is used for giveaway
                int numberOfViewers = 0;

                List<string> staffInChannel; //StaffInChannel will be used to load info of the staff
                List<string> peopleCanLink = new List<string>(); //List with people that can post links
                
                WarningList = new List<Warning>();

                GiveawayUsers = new List<string>(); //Initialize the GiveawayUsers list
                viewers = new ViewerList();
                Giveaways giveaway = new Giveaways(); //Initialize a new instance for the giveaway
                DateTime upTime = DateTime.Now;

                #endregion

                #region Thread Region

                if(_StartInfo.WantTimedMessage)
                    new Thread(TimingMessage).Start(); //Starting the TimingMessage function on another thread
                new Thread(() => Application.Run(viewers)).Start(); //Starting the viewer list form on another thread

                #endregion

                #region Loading Files Region

                /*
                 * Check if exist staffList file, if it exist, I load all info in the list.
                 * If it doesn't exist, I create new file and initialize new list
                 * */
                if (File.Exists(_StaffListFile))
                {
                    var info = new FileInfo(_StaffListFile);

                    staffInChannel = info.Length != 0
                        ? JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(_StaffListFile))
                        : new List<string>();
                }
                else
                {
                    File.Create(_StaffListFile);
                    staffInChannel = new List<string>();
                }


                /*
                 * Check if exist CustomCommand file, if it exist, I load all info in the list.
                 * If it doesn't exist, I create new file and initialize new list
                 * */
                if (File.Exists(_CustomCommandsFile))
                {
                    var info = new FileInfo(_CustomCommandsFile);

                    ComInfo = info.Length != 0
                        ? JsonConvert.DeserializeObject<List<CommandInfo>>(File.ReadAllText(_CustomCommandsFile))
                        : new List<CommandInfo>();
                }
                else
                {
                    File.Create(_CustomCommandsFile);
                    ComInfo = new List<CommandInfo>();
                }

                #endregion

                #region Bot Function Region
                
                //While there are command received from the server of Twitch
                while ((inputLine = reader.ReadLine()) != null)
                {
                    ConsoleIrc.WriteMessage(inputLine); //I write what command I received from the server

                    //If I know the command that I receive, I will work on it
                    if ((inputLine.Contains("JOIN") || inputLine.Contains("PART") || inputLine.Contains("PRIVMSG")) || inputLine.Contains("PING") && !inputLine.Contains("HISTORYEND"))
                    {
                        var commandIRC = String.Empty;

                        if (!inputLine.Contains("jtv"))
                        {
                            var start = inputLine.LastIndexOf("tmi.twitch.tv ") + 14; //Command start at this position 
                            var end = inputLine.IndexOf("#" + _StartInfo.Channel) - 1 - start;
                            //Last char of command is at this postion
                            commandIRC = inputLine.Substring(start, end); //So I found the command
                        }
                        else
                        {
                            if (inputLine.Contains("MODE"))
                                commandIRC = "MODE";
                            else if (inputLine.Contains("SPECIALUSER"))
                                commandIRC = "SPECIALUSER";
                        }

                        switch (commandIRC)
                        {
                            case "PRIVMSG": //Message wrote in the chat
                            {
                                //Parse nickname of person that send the message
                                var nickname = inputLine.Substring(1, inputLine.IndexOf("!") - 1);
                                var message = inputLine.Substring(inputLine.IndexOf(":", 1) + 1); //Parse message
                                var command = message.Split(' ')[0]; //Check for the commands that as been used

                                    #region !stream - Info about current stream

                                if (command == "!stream")
                                {
                                    try
                                    {
                                        //Download of infos about the channel from TwitchAPI, that use JSON
                                        var json =
                                            new WebClient().DownloadString(
                                                string.Format("https://api.twitch.tv/kraken/channels/{0}",
                                                    _StartInfo.Channel));
                                        //Deserialization of the JSON info
                                        dynamic channelInfos = JsonConvert.DeserializeObject(json);
                                        //Print out the info
                                        Irc.SendMessage("Today we are playing " + channelInfos["game"] + ". Enjoy");
                                    }
                                    catch (Exception)
                                    {
                                        Irc.SendMessage("Couldn't retrieve this info. Try later!");
                                    }
                                }
                                    #endregion

                                    #region !giveaway - Giveaway options ( Start and Stop )

                                else if (command == "!giveaway" && staffInChannel.Contains(nickname))
                                {
                                    if (message.Split(' ')[1] == "on") //If second word is on:
                                    {
                                        keyword = message.Split(' ')[2]; //Read the keyword want to set
                                        var time = Convert.ToInt32((message.Split(' ')[3]));
                                            //Read the duration time want to set
                                        giveaway.startGiveaway(keyword, time); //Start the giveaway
                                    }
                                    else if (message.Split(' ')[1] == "off") //Else, if second word is off
                                    {
                                        if (giveaway.timeOfGiveaway > 0) //If duration is > 0
                                            giveaway.stopGiveaway(); //Stop the giveaway
                                        else
                                            Irc.SendMessage("There's no giveaway active at the moment.");
                                    }
                                    else
                                        Irc.SendMessage(
                                            "Giveaway: to start a giveaway, use !giveaway on keyword time ( minutes ); to stop a giveaay, use !giveaway off.");
                                }
                                    #endregion

                                    #region Giveaway Partecipating Keyword

                                else if (message == keyword && giveaway.timeOfGiveaway > 0)
                                    //If the keyword is right and the duration is > 0
                                {
                                    if (!GiveawayUsers.Contains(nickname))
                                        //If the person is not in giveaway list already add him.
                                        GiveawayUsers.Add(nickname);
                                }
                                    #endregion

                                    #region !staffupdate - Allowing channel admin to update staff list

                                else if (message == "!staffupdate" && nickname == _StartInfo.Channel)
                                {
                                    Irc.SendCommand("/mods");
                                        //Send /mods to read know what mods have rights in this chat room
                                    staffInChannel = FillStaffList(reader.ReadLine()); //Fill the StaffList
                                    //Write it in the file
                                    File.WriteAllText(_StaffListFile,
                                        JsonConvert.SerializeObject(staffInChannel, Formatting.Indented));
#if DEBUG
                                    File.WriteAllText("../../" + _StaffListFile,
                                        JsonConvert.SerializeObject(staffInChannel, Formatting.Indented));
#endif
                                    Irc.SendMessage("Staff List Updated!");
                                }
                                    #endregion

                                    #region !spamfilter - Spam Filter

                                else if (command == "!spamfilter" && staffInChannel.Contains(nickname))
                                {
                                    var status = message.Split(' ')[1];
                                        //read the status to set on or off for the spam filter and save it back to file

                                    if (status == "on")
                                    {
                                        _StartInfo.SpamFilterStatus = true;
                                        Irc.SendMessage("Spam Filter Enabled!");
                                        File.WriteAllText(_StartInfoFile, JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                    }
                                    else if (status == "off")
                                    {
                                        _StartInfo.SpamFilterStatus = false;
                                        Irc.SendMessage("Spam Filter Disabled!");
                                        File.WriteAllText(_StartInfoFile, JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                    }
                                }
                                    #endregion

                                    #region !allowlink - Spam Filter Function to allow people post links

                                else if (command == "!allowlink" && _StartInfo.SpamFilterStatus)
                                {
                                    if (staffInChannel.Contains(nickname))
                                    {
                                        var userAllow = message.Split(' ')[1];
                                        peopleCanLink.Add(userAllow);
                                        Irc.SendMessage(
                                            string.Format(
                                                "{0}, now you can post one single link, after you will not have the power anymore.",
                                                userAllow));
                                    }
                                }
                                    #endregion

                                    #region Spam Filter for Links and E-Mails

                                else if (CheckSpam(message) && _StartInfo.SpamFilterStatus)
                                {
                                    if (!staffInChannel.Contains(nickname))
                                    {
                                        if (peopleCanLink.Contains(nickname))
                                            //If the person have already the permission, remove him from allowlist
                                            peopleCanLink.Remove(nickname);
                                        else
                                        {
                                            if (WarningList.Any(x => x.name == nickname && x.timeoutCount < 3))
                                            {
                                                Irc.SendCommand(string.Format("/timeout {0} 1", nickname));
                                                Irc.SendMessage(
                                                    string.Format(
                                                        "[WARNING] Dear {0}, next time you want to post a link, ask first the permission to mods!",
                                                        nickname));

                                                int userIndex = WarningList.FindIndex(x => x.name == nickname);

                                                Warning temp = WarningList[userIndex];
                                                temp.timeoutCount++;
                                                WarningList[userIndex] = temp;
                                            }
                                            else if (WarningList.Any(x => x.name == nickname && x.timeoutCount >= 3))
                                            {
                                                Irc.SendCommand(string.Format("/timeout {0} 3600", nickname));
                                                Irc.SendMessage(
                                                    string.Format(
                                                        "[BAN] Dear {0}, you are out of my stream chat for one hour! Learn the rules!",
                                                        nickname));
                                                WarningList.Remove(
                                                    WarningList[WarningList.FindIndex(x => x.name == nickname)]);
                                            }
                                            else
                                            {
                                                Irc.SendCommand(string.Format("/timeout {0} 1", nickname));
                                                Irc.SendMessage(
                                                    string.Format(
                                                        "[WARNING] Dear {0}, next time you want to post a link, ask first the permission to mods!",
                                                        nickname));
                                                Warning temp = new Warning();
                                                temp.name = nickname;
                                                temp.timeoutCount = 1;
                                                WarningList.Add(temp);
                                            }
                                        }
                                    }
                                }
                                    #endregion

                                    #region !time - Print in chat local and universal time

                                else if (command == "!time")
                                {
                                    Irc.SendMessage(
                                        string.Format(
                                            "{0} is the local time and {1} is the universal time.",
                                            DateTime.Now, DateTime.Now.ToUniversalTime()));
                                }
                                    #endregion

                                    #region !uptime - Print in chat the uptime of the bot ( usually the stream )

                                else if (command == "!uptime")
                                {
                                    //Calculate the difference of time between Bot startup and time now
                                    var timeSoFar = DateTime.Now - upTime;

                                    Irc.SendMessage(
                                        string.Format(
                                            "The stream is up from {0}:{1}!",
                                            timeSoFar.Hours, timeSoFar.Minutes));
                                }
                                    #endregion

                                    #region !addcommand - Add a custom command to the bot, the message have to be in "message"

                                else if (command == "!addcommand" && staffInChannel.Contains(nickname))
                                {
                                    var customCommandName = message.Split(' ')[1]; //Read the command name
                                    if (ComInfo.Any(x => x.commandName.Contains(customCommandName)) == false)
                                        //Check if it exist using LINQ
                                    {
                                        var ci = new CommandInfo();
                                        if (!customCommandName.Contains("!")) //If not contains this char, I add it
                                            customCommandName = "!" + customCommandName;
                                        ci.commandName = customCommandName;

                                        //Calculate the message
                                        var startText = message.IndexOf("\"") + 1;
                                        var endText = message.LastIndexOf("\"") - startText;
                                        var text = message.Substring(startText, endText);

                                        ci.text = text;
                                        ComInfo.Add(ci);
                                        File.WriteAllText(_CustomCommandsFile,
                                            JsonConvert.SerializeObject(ComInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _CustomCommandsFile,
                                            JsonConvert.SerializeObject(ComInfo, Formatting.Indented));
#endif

                                        Irc.SendMessage("Command added successfully!");
                                    }
                                    else
                                        Irc.SendMessage("Command already exist!");
                                }
                                    #endregion

                                    #region !delcommand - Delete a custom command from the bot

                                else if (command == "!delcommand")
                                {
                                    if (staffInChannel.Contains(nickname))
                                    {
                                        var customCommandName = message.Split(' ')[1]; //Read the command name
                                        if (ComInfo.Any(x => x.commandName.Contains(customCommandName)))
                                            //Check if it exist
                                        {
                                            var ci = new CommandInfo();

                                            if (!customCommandName.Contains("!")) //If there's no !, I add it
                                                customCommandName = "!" + customCommandName;

                                            ComInfo.RemoveAll(x => x.commandName == customCommandName);
                                            //Remove all commands with this name

                                            File.WriteAllText(_CustomCommandsFile,
                                                JsonConvert.SerializeObject(ComInfo, Formatting.Indented));
#if DEBUG
                                            File.WriteAllText("../../" + _CustomCommandsFile,
                                                JsonConvert.SerializeObject(ComInfo, Formatting.Indented));
#endif

                                            Irc.SendMessage("Command removed successfully!");
                                        }
                                        else
                                            Irc.SendMessage("Command not exist!");
                                    }
                                }
                                    #endregion

                                    #region Custom Commands Wrote in Chat

                                else if (ComInfo.Any(x => x.commandName == command))
                                    //Check if there's a command with that name, using LINQ
                                {
                                    //It has been found, print it out!
                                    var ci = ComInfo.Single(x => x.commandName == command);
                                    Irc.SendMessage(ci.text);
                                }
                                    #endregion

                                    #region !starttimedmessage - Start the timed message

                                else if (command == "!starttimedmessage" && staffInChannel.Contains(nickname))
                                {
                                    if (!_StartInfo.WantTimedMessage)
                                    {
                                        _StartInfo.WantTimedMessage = true;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                        //Starting the TimingMessage function on another thread
                                        new Thread(TimingMessage).Start();
                                    }
                                    else
                                        Irc.SendMessage("Timed Message already on!");
                                }
                                    #endregion

                                    #region !stoptimedmessage - Stop the timed message

                                else if (command == "!stoptimedmessage" && staffInChannel.Contains(nickname))
                                {
                                    if (_StartInfo.WantTimedMessage)
                                    {
                                        _StartInfo.WantTimedMessage = false;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                    }
                                    else
                                        Irc.SendMessage("Timed Message already off!");
                                }
                                    #endregion
                                    
                                    #region !changetime - How soon the message appear in chat?

                                else if (command == "!changetime" && staffInChannel.Contains(nickname))
                                {
                                    if (message.Split(' ')[1] != String.Empty)
                                    {
                                        _StartInfo.TimerForMessage = Convert.ToInt32(message.Split(' ')[1]);

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                    }
                                    else
                                        Irc.SendMessage(
                                            "Change Time: If you want to change the timed message, write the time in minutes");
                                }

                                #endregion

                                    #region !joinnotice - Notice in chat when a user join the chat room

                                else if (command == "!joinnotice" && staffInChannel.Contains(nickname))
                                {
                                    if (message.Split(' ')[1] == "on")
                                    {
                                        _StartInfo.JoinNotice = true;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                        Irc.SendMessage(
                                            "Join Notice Activated");
                                    }
                                    else if (message.Split(' ')[1] == "off")
                                    {
                                        _StartInfo.JoinNotice = false;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                        Irc.SendMessage(
                                            "Join Notice Deactivated");
                                    }
                                    else
                                        Irc.SendMessage(
                                            "Join Notice: If you want to noticed in chat, you have to use as second word on or off");
                                }
                                    #endregion

                                    #region !partnotice - Notice in chat when a user part the chat room

                                else if (command == "!partnotice" && staffInChannel.Contains(nickname))
                                {
                                    if (message.Split(' ')[1] == "on")
                                    {
                                        _StartInfo.PartNotice = true;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                        Irc.SendMessage(
                                            "Part Notice Activated");
                                    }
                                    else if (message.Split(' ')[1] == "off")
                                    {
                                        _StartInfo.PartNotice = false;

                                        File.WriteAllText(_StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#if DEBUG
                                        File.WriteAllText("../../" + _StartInfoFile,
                                            JsonConvert.SerializeObject(_StartInfo, Formatting.Indented));
#endif
                                        Irc.SendMessage(
                                            "Part Notice Deactivated");
                                    }
                                    else
                                        Irc.SendMessage(
                                            "Part Notice: If you want to noticed in chat, you have to use as second word on or off");
                                }

                                    #endregion

                                break;
                            }

                            case "JOIN": //People that joined the channel
                            {
                                //Parse nickname of person that joined the channel
                                var nickname = inputLine.Substring(1, inputLine.IndexOf("!") - 1);
                                numberOfViewers++;
                                viewers.Invoke(new MethodInvoker(() => { viewers.AddViewer(nickname); }));
                                viewers.Invoke(new MethodInvoker(() => { viewers.EditLabel(numberOfViewers); }));

                                /* 
                                 * When a user enter, try to register him in the database! 
                                 * Code 1:  Added succesfully!
                                 * Code -1: Already exist!
                                 * But really don't care about the codes.
                                */
                                var saveUser = new WebClient().DownloadString("http://qairsoft.altervista.org/saveuser.php?username=" + nickname);

                                if(_StartInfo.JoinNotice)
                                {
                                    if (!nickname.Contains(_StartInfo.Channel) && !nickname.Contains(_StartInfo.Nick.ToLower()))
                                    {
                                        // Welcome the nickname to channel by sending a notice
                                        Irc.SendMessage(string.Format("Hi {0} and welcome to {1} channel!", nickname, _StartInfo.Channel));
                                    }
                                }
                                break;
                            }

                            case "PART": //People that left the channel
                            {
                                var nickname = inputLine.Substring(1, inputLine.IndexOf("!") - 1);
                                    //Parse nickname of person that left the channel
                                numberOfViewers--;
                                viewers.Invoke(new MethodInvoker(() => { viewers.RemoveViewer(nickname); }));
                                viewers.Invoke(new MethodInvoker(() => { viewers.EditLabel(numberOfViewers); }));

                                if (_StartInfo.PartNotice)
                                {
                                    if (!nickname.Contains(_StartInfo.Channel) && !nickname.Contains(_StartInfo.Nick.ToLower()))
                                    {
                                        // Welcome the nickname to channel by sending a notice
                                        Irc.SendMessage(string.Format("Thanks for watching {0}!", nickname));
                                    }
                                }
                                break;
                            }

                            case "SPECIALUSER":  //Twitch Staff
                            case "MODE": //Staff
                            {
                                if (commandIRC == "MODE")
                                {
                                    var start = inputLine.IndexOf("+o") + 3;
                                    var length = inputLine.Length - start;
                                    var nickname = inputLine.Substring(start, length);

                                    staffInChannel.Add(nickname);
                                }
                                else if (commandIRC == "SPECIALUSER")
                                {
                                    var start = inputLine.IndexOf("SPECIALUSER") + 12;
                                    var length = inputLine.LastIndexOf(" ") - start;
                                    var nickname = inputLine.Substring(start, length);

                                    staffInChannel.Add(nickname);
                                }
                                break;
                            }

                            case "PING": //Reply Pong to Ping
                            {
                                Irc.PongServer(Server);
                                break;
                            }
                        }
                    }
                }

                #endregion

                // Close all streams
                Irc.Close();
                reader.Close();
                client.Close();
            }
            catch (Exception e)
            {
                //If error occur, I close myself after a command in console
                ConsoleIrc.WriteError(e.ToString());
                Console.ReadLine();

                Irc.Close();
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Fill Staff List for permissions
        /// </summary>
        /// <param name="staffList">Message received from Twitch with staff list</param>
        /// <returns>The List with all the staff</returns>
        private static List<string> FillStaffList(string staffList)
        {
            var length = staffList.LastIndexOf(':') + 1;
            staffList = staffList.Substring(length);
            staffList = staffList.Replace(" ", String.Empty);
            var list = staffList.Split(',');
            
            List<string> temp = new List<string>();
            temp.AddRange(list);
            temp.Add(_StartInfo.Channel.ToLower());

            return temp;
        }

        /// <summary>
        /// Checking the message to find for spam keyword!
        /// </summary>
        /// <param name="message">Message to check spam on</param>
        /// <returns>True if found spma, False if not</returns>
        private static bool CheckSpam(string message)
        {
            if ((message.Contains("http")    || 
                message.Contains("www.")     ||
                message.Contains(".com")     ||
                message.Contains(".it")      ||
                message.Contains(".net")     ||
                message.Contains(".org")     ||
                message.Contains(".tv") ) && !message.Contains("@"))    //Check if the message contains one of those illegale words
                    return true;
            return false;
        }

        /// <summary>
        /// This function will be used on another thread for Timed Message every minutes
        /// </summary>
        private static void TimingMessage()
        {
            const int msPerMin = 60000; //1000 ms = 1 s; 60000 ms = 1 m
            int minutes = _StartInfo.TimerForMessage * msPerMin;  //Minutes for thread sleep!

            while (_StartInfo.WantTimedMessage)
            {
                Thread.Sleep(minutes);
                Irc.SendMessage(_StartInfo.TimedMessage);
            }
        }
    }
}