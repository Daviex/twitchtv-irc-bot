﻿using System.Threading;

namespace TwitchTV_Bot
{
    /*
    * Class that sends PING to irc server every 15 seconds
    */
    public class PingSender
    {
        private readonly Thread _pingSender;

        // Empty constructor makes instance of Thread
        public PingSender()
        {
            _pingSender = new Thread(Run);
        }
        // Starts the thread
        public void Start()
        {
            _pingSender.Start();
        }

        // Send PING to irc server every 15 seconds
        public void Run()
        {
            while (true)
            {
                IrcBot.Irc.PingServer(IrcBot.Server);
                Thread.Sleep(15000);
            }
        }
    }
}
