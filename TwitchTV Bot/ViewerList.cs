﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TwitchTV_Bot
{
    public partial class ViewerList : Form
    {
        public ViewerList()
        {
            InitializeComponent();
        }

        public void AddViewer(string nickname)
        {
            lst_viewers.Items.Add(nickname);
        }

        public void RemoveViewer(string nickname)
        {
            lst_viewers.Items.Remove(nickname);
        }

        public void EditLabel(int number)
        {
            lbl_text.Text = "Viewers: " + number;
        }
    }
}
