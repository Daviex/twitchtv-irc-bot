﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace TwitchTV_Bot
{
    public class IrcWriter : StreamWriter
    {
        private readonly string _channel;

        public IrcWriter(NetworkStream stream, string channel) : base(stream)
        {
            _channel = channel;
            AutoFlush = true;
        }

        /// <summary>
        /// This function will send to the IRC server the password to login
        /// </summary>
        /// <param name="password">Password of the account ( Maybe oauth )</param>
        public void SendPassword(string password)
        {
            WriteLine("PASS " + password);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PASS " + password);
#endif
        }

        /// <summary>
        /// This function will send to the IRC server the username to login
        /// </summary>
        /// <param name="username">Username of the account</param>
        public void SendNick(string username)
        {
            WriteLine("NICK " + username);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - NICK " + username);
#endif
        }

        /// <summary>
        /// Send request to IRC server to join this channel
        /// </summary>
        public void JoinChannel()
        {
            WriteLine("JOIN #" + _channel);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - JOIN #" + _channel);
#endif
        }

        /// <summary>
        /// Send request to IRC server to leave this channel
        /// </summary>
        public void LeaveChannel()
        {
            WriteLine("PART #" + _channel);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PART #" + _channel);
#endif
        }

        /// <summary>
        /// Send a command that starts with /
        /// </summary>
        /// <param name="message">Message</param>
        public void SendCommand(string message)
        {
            WriteLine("PRIVMSG #" + _channel + " :" + message);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PRIVMSG #" + _channel + " :" + message);
#endif
        }
        
        /// <summary>
        /// Send the message on the chat
        /// </summary>
        /// <param name="message">Message</param>
        public void SendMessage(string message)
        {
            WriteLine("PRIVMSG #" + _channel + " :/me " + message);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PRIVMSG #" + _channel + " :/me " + message);
#endif
            // Sleep to prevent excess flood
            Thread.Sleep(50);
        }

        /// <summary>
        /// Send request to IRC server to have user list!
        /// </summary>
        public void RequestUserList()
        {
            WriteLine("WHO #" + _channel);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - WHO #" + _channel);
#endif
        }

        /// <summary>
        /// Ping to Pong of the server
        /// </summary>
        /// <param name="server">Hostname of the server</param>
        public void PingServer(string server)
        {
            WriteLine("PING :" + server);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PING :" + server);
#endif
        }

        /// <summary>
        /// Pong to Ping of the server
        /// </summary>
        /// <param name="server">Hostname of the server</param>
        public void PongServer(string server)
        {
            WriteLine("PONG :" + server);
#if DEBUG
            ConsoleIrc.WriteDebug("DEBUG - PONG :" + server);
#endif
        }
    }
}
