﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace TwitchTV_Bot
{
    public class Giveaways
    {
        public int timeOfGiveaway;

        public Giveaways()  {   }

        /// <summary>
        /// Start the giveaway
        /// </summary>
        /// <param name="keyword">Message to type to enter the giveaway</param>
        /// <param name="time">Duration of the giveaway</param>
        public void startGiveaway(string keyword, int time)
        {
            timeOfGiveaway = time;
            IrcBot.Irc.SendMessage(string.Format("The giveaway is on! The keyword is: {0}. You have {1} minutes to enter!", keyword, timeOfGiveaway));
            new Thread(new ThreadStart(TimerGiveaway)).Start();
        }

        /// <summary>
        /// Stop the giveaway first of the time
        /// </summary>
        public void stopGiveaway()
        {
            timeOfGiveaway = 0;
            decideWinner();
        }

        /// <summary>
        /// Thread will run until giveaway Timer doesn't reach 0
        /// </summary>
        public void TimerGiveaway()
        {
            const int msPerSec = 1000;
            timeOfGiveaway = timeOfGiveaway * 60; //From minutes to seconds

            while (timeOfGiveaway > 0)
            {
                timeOfGiveaway--;
                Thread.Sleep(msPerSec);
            }

            stopGiveaway();
        }

        /// <summary>
        /// Decide who is win and send it in the chat
        /// </summary>
        public void decideWinner()
        {
            var winner = whoWon(IrcBot.GiveawayUsers);
            if (winner != null)
                IrcBot.Irc.SendMessage(string.Format("Congratulations! The winner is {0}! You will receive a PM with the prize!", winner));
            else
                IrcBot.Irc.SendMessage(string.Format("Nobody entered the giveaway. Nobody wins."));
            IrcBot.GiveawayUsers.Clear();
        }

        /// <summary>
        /// Choose th winner
        /// </summary>
        /// <param name="giveawayUsers">Users that entered the giveaway</param>
        /// <returns>Winner</returns>
        private string whoWon(List<string> giveawayUsers)
        {
            Random rand = new Random(Environment.TickCount);

            int usersNumber = giveawayUsers.Count;
            string winner;

            if (usersNumber > 0)
                winner = giveawayUsers[rand.Next() % usersNumber];
            else
                winner = null;

            return winner;
        }
    }
}
